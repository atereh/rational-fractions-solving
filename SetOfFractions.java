import java.io.*;

import java.util.ArrayList;
import java.io.File;
import java.util.Scanner;

public class SetOfFractions {
    private static ArrayList<RationalFraction> setOfFractions;

    private RationalFraction maxFraction;
    private RationalFraction minFraction;

    private static int amountOfGreaterFractions;
    private static int amountOfSmallerFractions;
    private static double lastGreaterComparisson;
    private static double lastSmallerComparisson;

    public SetOfFractions() {
        this.setOfFractions = new ArrayList<RationalFraction>();
    }

    public SetOfFractions(String filePath) {
        this.setOfFractions = new ArrayList<RationalFraction>();

        File file = new File(filePath);
        Scanner inputFile;

        try {
            inputFile = new Scanner(file);

            while (inputFile.hasNextLine()) {
                int numerator = inputFile.nextInt();
                int denominator = inputFile.nextInt();

                RationalFraction fraction = new RationalFraction(numerator, denominator);

                this.addFraction(fraction);
            }
            inputFile.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void printToFile(String filePath) throws IOException
    {
        FileWriter writer = new FileWriter ("output.txt", false);

        for ( int i = 0; i < setOfFractions.size(); i++)
        {
            writer.write(Double.toString(setOfFractions.get(i).getValue()));
            writer.write("\n");
        }

        writer.flush();
    }

    public void addFraction(RationalFraction newFraction) {
        if (newFraction == null) {
            return;
        }
        setOfFractions.add(newFraction);

        if (setOfFractions.size() == 1) {
            maxFraction = newFraction;
            minFraction = newFraction;
        } else if (maxFraction.getValue() < newFraction.getValue()) {
            maxFraction = newFraction;
        } else if (minFraction.getValue() > newFraction.getValue()) {
            minFraction = newFraction;
        }
    }

    public RationalFraction getMaxFraction() {
        return maxFraction;
    }

    public RationalFraction getMinFraction() {
        return minFraction;
    }

    public int getNumberOfFractions() {
        return setOfFractions.size();
    }

    public double getFractionValueByIndex(int index) {
        return setOfFractions.get(index).getValue();
    }

    public int getFractionNumeratorByIndex(int index) { return setOfFractions.get(index).getNumerator(); }

    public int getFractionDenominatorByIndex(int index) { return setOfFractions.get(index).getDenominator(); }

    public int getSize() {
        return setOfFractions.size();
    }

    public static int countGreaterFractions(RationalFraction fraction) {
        if (fraction.getValue() == lastGreaterComparisson) { //по сути последнее сравнение мы оставляем в lastComparisson
            return  amountOfGreaterFractions;
        }

        amountOfGreaterFractions = 0;
        lastGreaterComparisson = fraction.getValue();

        setOfFractions.forEach(currentFraction -> {
            if (currentFraction.getValue() > lastGreaterComparisson) {
                amountOfGreaterFractions++;
            }
        });

        return amountOfGreaterFractions;
    }

    public static int countSmallerFractions(RationalFraction fraction) {
        if (fraction.getValue() == lastSmallerComparisson) {
            return  amountOfSmallerFractions;
        }

        amountOfSmallerFractions = 0;
        lastSmallerComparisson = fraction.getValue();

        setOfFractions.forEach(currentFraction -> {
            if (currentFraction.getValue() < lastSmallerComparisson) {
                amountOfSmallerFractions++;
            }
        });

        return amountOfSmallerFractions;
    }
}
