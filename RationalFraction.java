public class RationalFraction implements Cloneable {
    private int numerator;
    private int denominator;
    private double value;

    public RationalFraction(int numerator, int denominator) throws ArithmeticException {

        if (denominator == 0) {
            throw new ArithmeticException("Denominator cannot be zero");
        }

        this.numerator = numerator;
        this.denominator = denominator;

        this.value = (double) numerator / denominator;
    }
    
    public String getSimple() {
        int result = 0;
        int replacement = 0;

        int bigger = (numerator > denominator ? numerator : denominator);
        int smaller = (numerator < denominator ? numerator : denominator);

        while(result == 0) {
            if (bigger % smaller == 0) result = smaller;
            else replacement = bigger % smaller;
            bigger = (replacement > smaller ? replacement : smaller);
            smaller = (replacement < smaller ? replacement : smaller);
        }

        return numerator/result + "/" + denominator/result;
    }


    public double getValue() {
        return value;
    }

    public int getNumerator() {
        return numerator;
    }

    public int getDenominator() {
        return denominator;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {  
        return super.clone();
    } 
}