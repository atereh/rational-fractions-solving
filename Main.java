import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        SetOfFractions firstSet = new SetOfFractions("./input.txt");
        RationalFraction fraction = new RationalFraction(5, 6);
        System.out.println("1-st fraction value = " + fraction.getValue());

        System.out.println("min fraction in a row = " + firstSet.getMinFraction().getValue() + "\nmax fraction in a row = " + firstSet.getMaxFraction().getValue());
        System.out.println("number of fractions greater than " + fraction.getValue() + " = " + SetOfFractions.countGreaterFractions(fraction));

        Polynomial poly = new Polynomial(firstSet);
        Polynomial poly2 = new Polynomial(firstSet);
        Polynomial poly3 = Polynomial.add(poly, poly2);
        System.out.println("\npolynomial value = " + poly3.calculate(1.5));

        RationalFraction fraction2 = new RationalFraction(5, 6);
        System.out.println("number of fractions smaller = " + SetOfFractions.countSmallerFractions(fraction2));

        try {
            firstSet.printToFile("./out.txt");

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}