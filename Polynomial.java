import java.util.ArrayList;

public class Polynomial {
    private ArrayList<Double> coefficients;

    public Polynomial(SetOfFractions coefficients) {
        this.coefficients = new ArrayList<Double>();
        for (int i = 0; i < coefficients.getNumberOfFractions(); i++)
            this.coefficients.add(coefficients.getFractionValueByIndex(i));
    }

    public Polynomial() {
        this.coefficients = new ArrayList<Double>();
    }

    public double calculate(double x) {
        double result = 0;

        for (int i = 0; i < coefficients.size(); ++i) {
            result += coefficients.get(i) * Math.pow(x, coefficients.size() - i);
        }
        return result;
    }

    public double calculate2(double x) {
        double result = 0;

        for (int i = 0; i < coefficients.size(); i++) {
            result += coefficients.get(i) * Math.pow(x, i);
        }
        return result;
    }

    public static Polynomial add(Polynomial firstPolynomial, Polynomial secondPolynomial) {
        Polynomial resultedPolynomial = new Polynomial();

        for (int index = 0; index < firstPolynomial.coefficients.size() && index < secondPolynomial.coefficients.size(); index++) {
            resultedPolynomial.coefficients.add(firstPolynomial.coefficients.get(index) + secondPolynomial.coefficients.get(index));
        }
        return resultedPolynomial;
    }
}
