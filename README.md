**Project description**

This is an academical course's project with several classes and simple methods 

---

## Project solutions

1. Given some set of rational fractions
2. Count double value of each fraction
3. Count number of fractions smaller than exact fraction
4. Count number of fractions greater than exact fraction
5. Count polynom of fractions
6. Provide each fraction in it's simplest form

---
